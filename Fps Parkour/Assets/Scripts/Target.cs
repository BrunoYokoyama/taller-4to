﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public int score = 10;
    public float timeToDestoy;
    public void DestroyThis()
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        Invoke("DestroyThis", timeToDestoy);
    }
}
