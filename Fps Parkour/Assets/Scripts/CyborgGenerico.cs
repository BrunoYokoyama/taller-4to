﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CyborgGenerico : MonoBehaviour
{
    public BasicEnemy enemy;
    public BoxCollider punchCollider;
    public float punchDistance,punchCooldown;
    public bool isPunching,canPunch,embistiendo;

    public float embestirTimer,chaseRange;
    public float embestirCooldown,embestidaDuration;
    public bool isMoving;
    public Animator animator;

    public float dañoEmbestida=60f;
    public float dañoPunch=40f;
    private void Start()
    {
        enemy = GetComponent<BasicEnemy>();
        punchCooldown = 1.5f;
        canPunch = true;
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        enemy.Die();     
        GolpeFrio();
        Embestir();
        SetAnimation();

        if (enemy.distanceToPlayer <= chaseRange&&!isPunching)
        {
            enemy.Chase();
            enemy.agent.isStopped = false;
            isMoving = true;
        }
        else
        {
            enemy.agent.isStopped = true;
            isMoving = false;
        }
        
    }

    void GolpeFrio()
    {
        punchDistance = punchCollider.size.z;
        if (enemy.distanceToPlayer <= 4f&&canPunch)
        {
            Debug.Log("Golpeando...");
            
            punchCollider.enabled = true;
            canPunch = false;
            isPunching = true;
            Invoke("ResetPunch", punchCooldown);
        }
    }

    void ResetPunch()
    {
        
        punchCollider.enabled = false;
        isPunching = false;
        canPunch = true;
    }

    void Embestir()
    {
        embestirTimer += Time.deltaTime;
        
        if (embestirTimer >= embestirCooldown)
        {
            canPunch = false;
            enemy.agent.speed = enemy.normalSpeed * 2.5f;
            embistiendo = true;
            Invoke("StopEmbestida", embestidaDuration);
        }
    }
    void StopEmbestida()
    {
        canPunch = true;
        embestirTimer = 0;
        enemy.agent.speed = enemy.normalSpeed;
        embistiendo = false;
    }

    void SetAnimation()
    {
        animator.SetBool("Embistiendo", embistiendo);
        animator.SetBool("canChase", isMoving);
        animator.SetBool("Punching", isPunching);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isPunching)
            {
                Debug.Log("PlayerHit");
                other.GetComponent<PlayerController>().health -= dañoPunch;
                other.GetComponentInParent<PlayerController>().damageTaken += dañoPunch;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            if (embistiendo)
            {
                collision.collider.GetComponent<PlayerController>().health -=dañoEmbestida;
                collision.collider.GetComponentInParent<PlayerController>().damageTaken += dañoEmbestida;
                StopEmbestida();
            }
        }
    }



}
