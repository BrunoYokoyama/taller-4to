﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LASERTEST : MonoBehaviour
{
    public LineRenderer lr;
    public float laserRange;
    public Transform laserPoint;
    public Transform target;
    void Start()
    {
        lr = GetComponent<LineRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        lr.SetPosition(0, transform.position);
        laserPoint.position = target.position;
        

        lr.SetPosition(1,laserPoint.position);

        //Quaternion rotTarget = Quaternion.LookRotation( target.position- transform.position);
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, rotTarget, 20 * Time.deltaTime);
    }
}
