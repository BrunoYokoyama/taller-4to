﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    public int damage, magazineSize, bulletsPerTap, bulletsLeft, bulletShot;
    public float timeBShoot, spread, range, /*reloadTime*/ timeBShoting;
    public bool allowButtongHold;

    bool shooting, readyToShoot, reloading;

    public Camera mainCamera;
    public Transform attackPoint;
    public RaycastHit rayHit;
    public LayerMask whatIsEnemy;

    

    public GameObject muscleFlash, bullethole;

    public GameObject player;
    
    //public int score;
    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
        player = GameObject.Find("Player");
    }
    private void Update()
    {
        Inputs();
        BulletCap();
       
    }
    void BulletCap()
    {
        if (bulletsLeft > magazineSize)
        {
            bulletsLeft = magazineSize;
        }
    }
    void Inputs()
    {
        if (allowButtongHold)
            shooting = Input.GetMouseButton(0);
        else
            shooting = Input.GetMouseButtonDown(0);

        //if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
        //    Reload();


        //-----Shooting-----

        if (readyToShoot && shooting /*&& !reloading*/ && bulletsLeft > 0)
        {
            bulletShot = bulletsPerTap;
            Shoot();
            
            //StartCoroutine(cameraShake.Shake(.15f,.2f));
            //GameObject obj = Instantiate(muscleFlash, attackPoint.transform.position, attackPoint.transform.rotation);
        }
            
        
    }

    //private void Reload()
    //{
    //    reloading = true;
    //    Invoke("ReloadFinished", reloadTime);
    //}

    //private void ReloadFinished()
    //{
    //    bulletsLeft = magazineSize;
    //    reloading = false;
    //}
    private void Shoot()
    {
        readyToShoot = false;
        player.GetComponent<PlayerController>().disparosRealizados++;
        //----Spread----

        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 direction = mainCamera.transform.forward + new Vector3(x, y, 0f);

        // -----RayCast------

        if(Physics.Raycast(mainCamera.transform.position,direction,out rayHit, range, whatIsEnemy))
        {
            
            Debug.Log(rayHit.collider.name);

            if (rayHit.collider.CompareTag("Enemy"))
            {
                Debug.Log("enemy Hit...");
                player.GetComponent<PlayerController>().timeToResetSobreCarga = 0;
                rayHit.collider.GetComponent<BasicEnemy>().health -= damage;
                Instantiate(bullethole, rayHit.point, Quaternion.identity);
            }

            //if (rayHit.collider.CompareTag("Target"))
            //{
            //    score += rayHit.collider.GetComponent<Target>().score;
            //    rayHit.collider.GetComponent<Target>().DestroyThis();

            //}
            //if (rayHit.collider.CompareTag("Breakable"))
            //{
            //    GameObject box = Instantiate(rayHit.collider.GetComponent<BreakableBox>().shatteredBox, rayHit.collider.gameObject.transform.position,
            //    rayHit.collider.gameObject.transform.rotation);

            //    Rigidbody rb = box.GetComponent<Rigidbody>();
            //    rb.AddExplosionForce(10f, box.transform.position, 0.2f, 0.2f, ForceMode.Impulse);
            //    Destroy(rayHit.collider.gameObject);

            //}

            

        }
        if(Physics.Raycast(mainCamera.transform.position, direction, out rayHit, range))
        {
            Instantiate(bullethole, rayHit.point, Quaternion.identity);
        }

        
        bulletsLeft--;
        bulletShot--;
        Invoke("ResetShoot",timeBShoting);
        if(bulletShot>0&&bulletsLeft>0)
        Invoke("Shoot", timeBShoot);
    }

    private void ResetShoot()
    {
        readyToShoot = true;
    }
}
