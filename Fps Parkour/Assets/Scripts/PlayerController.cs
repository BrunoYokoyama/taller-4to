﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    public float speed;
    public float basicSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float fallMultiplier = 2.5f;
    [SerializeField] private float lowJumpMultiplier = 2f;
    [SerializeField] private float jumpRaycastDistance;
    [SerializeField] private float MouseSensitivity;
    private float xRotation;
    [SerializeField] private LayerMask whatIsGround;
    public Rigidbody rb;
    public Camera firstPersonCamera;

    
   
     public bool isSprinting;
     public bool isMoving;

    public CapsuleCollider standCollider;
    public CapsuleCollider slideCollider;

    [SerializeField] private float slideDuration;
    public bool isSliding;
    public GameObject slideCameraPosition;
    public GameObject standCameraPosition;

    [SerializeField] private float basicFOV;
    private float startSlideTimer;

    public WallRunning wr;

    [Header("Stats")]
    public float health;
    public float maxHealth;
    public float sobreCarga;
    public float maxSobreCarga;
    public float timeToResetSobreCarga;

    public float damageTaken;
    public float prevHealth;
    public float disparosRealizados;

    public bool playerAlive = true;
    

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        wr = GetComponent<WallRunning>();
        basicSpeed = speed;
        slideCollider.enabled = false;
        basicFOV = firstPersonCamera.fieldOfView;
        health = 100;
        damageTaken = 0;
        maxHealth = 100;
        sobreCarga = 0;
        maxSobreCarga = 10;
    }

    private void Update()
    {
        if (playerAlive)
        {
            Jump();
            Rotate();
            StatsCap();
            if (Input.GetMouseButtonDown(1))
            {
                health -= 10;
            }
        }
        
    }

    private void FixedUpdate()
    {
        if (playerAlive)
        {
            Movement();
            Sprinting();
            Slide();
        }     
    }

    #region Movement
    private void StartSlide()
    {
        if (isSliding)
        {

            startSlideTimer += Time.deltaTime;
            standCollider.enabled = false;
            slideCollider.enabled = true;
            speed = basicSpeed * 2.5f;

            if (firstPersonCamera.fieldOfView < 70f)
            {
                firstPersonCamera.fieldOfView += 20f * Time.deltaTime;
            }
            firstPersonCamera.transform.parent = slideCameraPosition.transform;
            firstPersonCamera.transform.position = slideCameraPosition.transform.position;
            Invoke("StopSlide", slideDuration);

        }
        else
        {
            startSlideTimer = 0;
        }


    }

    private void StopSlide()
    {

        isSliding = false;
        standCollider.enabled = true;
        slideCollider.enabled = false;
        if (isSprinting)
        {
            speed = (basicSpeed * 2f) + sobreCarga;
        }
        else
        {
            speed = basicSpeed;
        }


        if (firstPersonCamera.fieldOfView > basicFOV && !isSliding)
        {
            firstPersonCamera.fieldOfView -= 100 * Time.deltaTime;
        }
        firstPersonCamera.transform.parent = standCameraPosition.transform;
        firstPersonCamera.transform.position = standCameraPosition.transform.position;
    }
    private void Jump()
    {
        if (rb.velocity.y < 0 || wr.isWallRunning)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            //rb.AddForce(Vector3.up * Physics.gravity.y * (fallMultiplier - 1 * Time.deltaTime), ForceMode.Impulse);
        }
        else if ((rb.velocity.y > 0 || wr.isWallRunning) && !Input.GetKey(KeyCode.Space))
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            //rb.AddForce(Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1 * Time.deltaTime), ForceMode.Impulse);
        }
        //rb.AddForce(Vector3.down * 40f);
        if ((Input.GetKeyDown(KeyCode.Space) && IsGrounded()))
        {
            rb.velocity = Vector3.up * jumpForce;
            //rb.AddForce(jumpForce * Vector3.up,ForceMode.Impulse);

        }



        if (wr.wallRunRight && (Input.GetKeyDown(KeyCode.Space) /*|| Input.GetKeyDown(KeyCode.A)*/))
        {

            rb.velocity = (-wr.orientation.right) * jumpForce * 1.5f;
            //rb.AddForce(Vector3.up * (jumpForce / 2), ForceMode.Impulse);
            rb.velocity = Vector3.up * jumpForce;

        }

        if (wr.wallRunLeft && (Input.GetKeyDown(KeyCode.Space) /*|| Input.GetKeyDown(KeyCode.D)*/))
        {

            rb.velocity = (wr.orientation.right) * jumpForce * 1.5f;
            //rb.AddForce(Vector3.up * (jumpForce / 2), ForceMode.Impulse);
            rb.velocity = Vector3.up * jumpForce;

        }

        if (wr.isWallRunning && !Input.GetKey(KeyCode.Space))
        {
            rb.velocity = Vector3.up * 0;
        }

    }
    private void Sprinting()
    {
        if (IsGrounded() && Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W))
        {
            StartSprint();
        }
        else if (!Input.GetKey(KeyCode.W) && isSprinting)
        {
            Invoke("StopSprint", 1f);
        }

    }

    private void StartSprint()
    {
        isSprinting = true;

        if (speed < (basicSpeed * 2f) + sobreCarga)
        {
            speed = basicSpeed * 2f + sobreCarga;
        }
    }
    private void StopSprint()
    {
        isSprinting = false;
        if (!isSliding)
            speed = basicSpeed + sobreCarga;

    }
    private void Rotate()
    {
        float x = Input.GetAxisRaw("Mouse X") * MouseSensitivity * Time.deltaTime;
        float y = Input.GetAxisRaw("Mouse Y") * MouseSensitivity * Time.deltaTime;

        transform.Rotate(Vector3.up * x);
        xRotation -= y;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        firstPersonCamera.transform.localRotation = Quaternion.Euler(xRotation, 0f, wr.cameraTilt);


    }
    private bool IsGrounded()
    {
        Debug.DrawRay(transform.position, Vector3.down * jumpRaycastDistance, Color.red);

        return Physics.Raycast(transform.position, Vector3.down, jumpRaycastDistance);
        
    }

    private void Movement()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical");

        Vector3 moveDirection;
        Vector3 yVelFix = new Vector3(0, rb.velocity.y, 0);
        moveDirection = (x * transform.right + z * transform.forward).normalized;
        rb.velocity = moveDirection * (speed + sobreCarga) * Time.deltaTime;
        rb.velocity += yVelFix;
        if (moveDirection != Vector3.zero)
        {
            isMoving = true;
        }
        else
            isMoving = false;

        //Vector3 movement = new Vector3(x, 0, z).normalized * (speed + sobreCarga) * Time.deltaTime;
        //if (movement != Vector3.zero)
        //    isMoving = true;
        //else
        //    isMoving = false;
        //Vector3 desiredPosition = rb.position + rb.transform.TransformDirection(movement);

        //rb.MovePosition(desiredPosition);

    }

    private void Slide()
    {
        if (speed != basicSpeed && Input.GetKey(KeyCode.LeftControl) && IsGrounded())
        {
            StartSlide();
            isSliding = true;
        }
    }
    #endregion

    #region Stats
    void StatsCap()
    {
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        if (sobreCarga > maxSobreCarga)
        {
            sobreCarga = maxSobreCarga;
        }
        if (sobreCarga > 0)
        {
            timeToResetSobreCarga += Time.deltaTime;
            if (timeToResetSobreCarga >= 5f)
            {
                timeToResetSobreCarga = 0;
                sobreCarga = 0;
            }
        }
    }


    public void SobreCarga()
    {

        sobreCarga++;
    }
    #endregion


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DeathBox"))
        {
            health = 0;
        }
    }
}
