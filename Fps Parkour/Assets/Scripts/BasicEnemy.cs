﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BasicEnemy : MonoBehaviour
{
    public float health;
    public GameObject player;
    public NavMeshAgent agent;
    public float distanceToPlayer;
    public float maxHealth;
    
    public float normalSpeed;
    void Start()
    {
        health = 100f;
        player = GameObject.Find("Player");
       
        normalSpeed = GetComponent<NavMeshAgent>().speed;
        
    }
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
    }

    public void Die()
    {
        if (health <= 0)
        {
            player.GetComponent<PlayerController>().SobreCarga();
            Destroy(gameObject);
        }
    }

    public void Chase()
    {      
        agent.SetDestination(player.transform.position);
        
    }
}
