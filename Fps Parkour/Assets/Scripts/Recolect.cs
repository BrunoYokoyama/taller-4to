﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recolect : MonoBehaviour
{
    public GameObject player,pistola,rifle,escopeta,uiManager;
    [HideInInspector]
    public string name;
    
    public float playerHealth;
    public float playerMaxHealth;

    public float pistolaCurrentAmmo;
    public float pistolaMaxAmmo;
    public float rifleCurrentAmmo;
    public float rifleMaxAmmo;
    public float escopetaCurrentAmmo;
    public float escopetaMaxAmmo;

    public GameObject notePanel;
    public Text noteTitleText;
    public Text noteText;
    public bool noteActive;
    void Start()
    {
        player = GameObject.Find("Player");
        uiManager = GameObject.Find("UIManager");
    }

    // Update is called once per frame
    void Update()
    {
        playerHealth = player.GetComponent<PlayerController>().health;
        playerMaxHealth = player.GetComponent<PlayerController>().maxHealth;

        pistolaCurrentAmmo = pistola.GetComponent<GunController>().bulletsLeft;
        pistolaMaxAmmo = pistola.GetComponent<GunController>().magazineSize;

        rifleCurrentAmmo = rifle.GetComponent<GunController>().bulletsLeft;
        rifleMaxAmmo = rifle.GetComponent<GunController>().magazineSize;

        escopetaCurrentAmmo = escopeta.GetComponent<GunController>().bulletsLeft;
        escopetaMaxAmmo = escopeta.GetComponent<GunController>().magazineSize;

        NoteActive();
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            name = other.GetComponent<Item>().name;

            switch (name)
            {
                case "Health":
                    if (playerHealth < playerMaxHealth)
                    {
                        player.GetComponent<PlayerController>().health += 50f;
                        Destroy(other.gameObject);
                        Debug.Log("Healing...");
                    }
                    break;

                case "Ammo":
                    if ((pistolaCurrentAmmo < pistolaMaxAmmo) || (rifleCurrentAmmo < rifleMaxAmmo) ||
                        (escopetaCurrentAmmo < escopetaMaxAmmo))
                    {
                        pistola.GetComponent<GunController>().bulletsLeft += 20;
                        rifle.GetComponent<GunController>().bulletsLeft += 50;
                        escopeta.GetComponent<GunController>().bulletsLeft += 25;
                        Debug.Log("Refilling ammo...");
                        Destroy(other.gameObject);
                    }
                    break;
                case "Note":
                    notePanel.SetActive(true);
                    noteTitleText.text=""+ other.GetComponent<Item>().noteTitle;
                    noteText.text=""+ other.GetComponent<Item>().noteText;
                    noteActive = true;
                    break;
                case "Objective":
                    uiManager.GetComponent<UIManager>().CompleteLevel();
                    break;

                case "SecondaryObjective":
                    other.GetComponent<Item>().primaryObjective.SetActive(true);
                    Destroy(other.gameObject);
                    break;
            }


        }
        else
        {
            return;
        }
    }

    void NoteActive()
    {
        if (noteActive)
        {
            Time.timeScale = 0;
            if (Input.GetKey(KeyCode.E))
            {
                Time.timeScale = 1;
                noteActive = false;
                notePanel.SetActive(false);
            }
        }
    }
}
