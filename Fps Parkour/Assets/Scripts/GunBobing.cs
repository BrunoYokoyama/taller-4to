﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBobing : MonoBehaviour
{
    public Transform weaponParent;
    public Vector3 initialPosition;
    public GameObject player;

    public float z;
    public float xIntensity;
    public float yIntensity;
    void Start()
    {
        initialPosition = transform.localPosition;
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetComponent<PlayerController>().isMoving)
        {
            z += Time.deltaTime*5;
            
        }else if (player.GetComponent<PlayerController>().isSprinting&&player.GetComponent<PlayerController>().isMoving)
        {
            z += Time.deltaTime * 8;
        }
        Bobing();
    }

    void Bobing()
    {
        
        weaponParent.localPosition = initialPosition+ new Vector3(Mathf.Cos(z)*xIntensity,0,Mathf.Sin(z)*yIntensity);
    }
}
