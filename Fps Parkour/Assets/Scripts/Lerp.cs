﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerp : MonoBehaviour
{
    public Transform starMarker;
    public Transform endMarker;

    public float speed = 1f;
    public float journeyLenght = 1f;
    public float startTime;

    public bool loop;

    //public Light light;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Lerping();
        //Lerping2();
        //PingPongLight();
    }

    void Lerping()
    {
        if (!loop)
        {
            float distCovered = (Time.time - startTime) * speed;
            transform.position = Vector3.Lerp(starMarker.position, endMarker.position, distCovered / journeyLenght);
            Debug.Log(distCovered);
        }
        if (loop)
        {
            float distCovered = Mathf.PingPong(Time.time - startTime, journeyLenght / speed);
            transform.position = Vector3.Lerp(starMarker.position, endMarker.position, distCovered / journeyLenght);
            Debug.Log(distCovered);
        }
    }

    void Lerping2()
    {
        float distCovered = Mathf.PingPong(Time.time, journeyLenght);
        transform.position = Vector3.Lerp(starMarker.position, endMarker.position, distCovered/journeyLenght);
    }
    void PingPongLight()
    {
        //light.intensity = Mathf.PingPong(Time.time, 4);
    }
}
