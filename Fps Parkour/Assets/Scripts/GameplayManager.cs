﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    public int nivelActual;
    public bool objetivoCumplido;
    public GameObject uiManager;
    void Start()
    {
        nivelActual = SceneManager.GetActiveScene().buildIndex;
    }

    // Update is called once per frame
    void Update()
    {
        ObjetivoCumplido();
    }

    void ObjetivoCumplido()
    {
        if (objetivoCumplido)
        {
            uiManager.GetComponent<UIManager>().CompleteLevel();
        }
    }
}
