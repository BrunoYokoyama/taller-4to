﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    public GameObject player;
    public float playerHealth;
    public float playerMaxHealth;
    public Image healthBar;

    public Text bullets;
    public GameObject bulletsCanvas;

    public GameObject pistola,rifle,escopeta,weaponManager;

    public Image[] weapon;

    private Color selectedColor;
    private Color unselectedColor;

    private int currentGun;

    public bool gameIsPaused;
    public GameObject pausePanel;
    public Text objectiveText;

    public bool menuScene;

    public GameObject deathPanel;

    public Camera mainCamera;
    public Animator cameraAnimator;
    private bool playerDead;

    public GameObject victoryPanel;
    public Text tiempoDeJuego;
    public Text dañoRecibido;
    public Text disparosRealizados;

    public float tiempoTotal;

    public bool levelFinished;

    void Start()
    {
        player = GameObject.Find("Player");
        pistola = GameObject.Find("Pistola");
        rifle = GameObject.Find("Rifle");
        escopeta = GameObject.Find("Escopeta Avanzada");
        weaponManager = GameObject.Find("Weapon Manager");
        bulletsCanvas = GameObject.Find("Weapon Canvas");

        selectedColor.a = 0.8f;
        unselectedColor.a = 0.4f;

        gameIsPaused = false;
        deathPanel.SetActive(false);
        Time.timeScale = 1f;
        if (!menuScene)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        Objetivos();
        
    }

    // Update is called once per frame
    void Update()
    {
        currentGun = weaponManager.GetComponent<WeaponManager>().currentGun;
        
        PauseMenu();

        if (player.GetComponent<PlayerController>().health <= 0)
        {
            playerDead = true;
            PlayerDead();
        }
        else
            deathPanel.SetActive(false);

        if (Input.GetKeyDown(KeyCode.P))
        {
            CompleteLevel();
        }
        tiempoTotal += Time.deltaTime;
    }
    private void LateUpdate()
    {
        UpdateHealthBar();
        ShowBullets();
        ShowActiveWeapon();
    }

    #region HUD
    void UpdateHealthBar()
    {
        playerMaxHealth = player.GetComponent<PlayerController>().maxHealth;
        playerHealth = player.GetComponent<PlayerController>().health;

        healthBar.fillAmount = playerHealth / playerMaxHealth;
    }

    void ShowBullets()
    {
        switch (weaponManager.GetComponent<WeaponManager>().currentGun)
        {
            case 1:

                bullets.text = "" + weaponManager.GetComponent<WeaponManager>().currentBullets;
                break;

            case 2:

                bullets.text = "" + weaponManager.GetComponent<WeaponManager>().currentBullets;
                break;

            case 3:

                bullets.text = "" + weaponManager.GetComponent<WeaponManager>().currentBullets;
                break;
        }
    }

    void ShowActiveWeapon()
    {
        switch (weaponManager.GetComponent<WeaponManager>().currentGun)
        {
            case 1:
                weapon[0].GetComponent<Image>().color = selectedColor;
                weapon[1].GetComponent<Image>().color = unselectedColor;
                weapon[2].GetComponent<Image>().color = unselectedColor;
                break;
            case 2:
                weapon[0].GetComponent<Image>().color = unselectedColor;
                weapon[1].GetComponent<Image>().color = selectedColor;
                weapon[2].GetComponent<Image>().color = unselectedColor;
                break;
            case 3:
                weapon[0].GetComponent<Image>().color = unselectedColor;
                weapon[1].GetComponent<Image>().color = unselectedColor;
                weapon[2].GetComponent<Image>().color = selectedColor;
                break;
        }
    }
    #endregion

    #region Pause
    void PauseMenu()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0 && Input.GetKeyDown(KeyCode.Escape)&&player.GetComponent<PlayerController>().health>0)
        {
            if (!gameIsPaused)
            {
                Time.timeScale = 0f;
                pausePanel.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                gameIsPaused = true;
            }
            else if (gameIsPaused)
            {
                Time.timeScale = 1f;
                pausePanel.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                gameIsPaused = false;
            }
        }
    }
    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        gameIsPaused = false;
        Debug.Log("Resumiendo...");
    }
    public void ReloadScene()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        gameIsPaused = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }
    public void QuitGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        gameIsPaused = false;
        SceneManager.LoadScene(0);
    }
    #endregion

    #region PlayerDead
    void EndLevel()
    {
        
        Time.timeScale = 0.2f;
        deathPanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
             
    }
    void PlayerDead()
    {
        player.GetComponent<PlayerController>().playerAlive = false;
        EndLevel();
        
    }

    #endregion

    #region CompleteLevel
    public void CompleteLevel()
    {
        victoryPanel.SetActive(true);
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        ShowPerformance();
    }
    public void Continuar()
    {
        Time.timeScale = 1f;
        victoryPanel.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        
    }

    public void ShowPerformance()
    {
        tiempoDeJuego.text = "TIEMPO: " + (int)tiempoTotal;
        dañoRecibido.text = "DAÑO RECIBIDO: " + (int)player.GetComponent<PlayerController>().damageTaken;
        disparosRealizados.text="DISPAROS REALIZADOS: "+player.GetComponent<PlayerController>().disparosRealizados;
    }

    #endregion

    #region Objetivos
    void Objetivos()
    {
        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 1:
                objectiveText.text = "Llega al final del tutorial";
                break;
            case 2:
                objectiveText.text = "Consigue el objetivo de alto valor. Luego llega al punto de extracción";
                break;
        }
    }
    #endregion

   
   
}
