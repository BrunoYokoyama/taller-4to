﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [Header("Spawn Values")]
    public GameObject[] enemy;
    float xPosition;
    float zPosition;
    [Range(0,1000)]
    public float areaX;
    [Range(0,1000)]
    public float areaZ;

    public float rounds;
    [Range(1,20)]
    public int enemiesToSpawn;
    [Range(1, 20)]
    public float timeToSpawn;

    public float spawnTimer;

    [Header("Prevent Overlapping")]
    public Collider[] collider;
    public float radius;
    public LayerMask ground;
    void Start()
    {
        SpawnEnemy(enemiesToSpawn);
    }

    // Update is called once per frame
    void Update()
    {
        
        
        spawnTimer += Time.deltaTime;
        if (spawnTimer >= timeToSpawn)
        {
            spawnTimer = 0;
            SpawnEnemy(enemiesToSpawn);
            rounds--;
            
        }
        DeactivateSpawner();
    }

   

    void SpawnEnemy(float enemiesToSpawn)
    {
        Vector3 spawnPos=transform.position;
        bool canSpawnHere=false;
        int safetyNet = 0;
        
        for (int i = 0; i <= enemiesToSpawn; i++)
        {
            while (!canSpawnHere)
            {
                xPosition = Random.Range(-areaX, areaX);
                zPosition= Random.Range(-areaZ, areaZ);
                
                spawnPos = new Vector3(xPosition, 1, zPosition);
                canSpawnHere = PreventSpawnOverlap(spawnPos);

                if (canSpawnHere)
                {

                    break;
                }
                safetyNet++;
                if (safetyNet > 50)
                {
                    break;
                    Debug.Log("Too many attempts");
                }
            }
                
           float enemyToSpawn = Random.Range(0, enemy.Length);
           GameObject newEnemy = Instantiate(enemy[(int)enemyToSpawn], spawnPos, Quaternion.identity);

            xPosition = Random.Range(-areaX, areaX);
            zPosition = Random.Range(-areaZ, areaZ);

            spawnPos = new Vector3(xPosition, 1, zPosition);

        }
       
        
        
        
    }
    bool PreventSpawnOverlap(Vector3 spawnPos)
    {
        collider = Physics.OverlapSphere(transform.position, radius, ground);
        for (int i = 0; i < collider.Length; i++)
        {
            Vector3 centerPoint = collider[i].bounds.center;
            float width = collider[i].bounds.extents.x;
            float depth = collider[i].bounds.extents.z;

            float leftExtent = centerPoint.x - width;
            float rightExtent = centerPoint.x + width;
            float forwardExtent = centerPoint.z + depth;
            float backExtent = centerPoint.z -depth;

            if (spawnPos.x >= leftExtent && spawnPos.x <= rightExtent)
            {
                if (spawnPos.z >= forwardExtent && spawnPos.z <= backExtent)
                {
                    return false;
                    
                }
            }
            
        }
        return true;
    }

    void DeactivateSpawner() 
    {
        if (rounds <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
