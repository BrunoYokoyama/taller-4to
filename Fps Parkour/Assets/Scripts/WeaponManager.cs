﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public GameObject pistola, rifle, escopeta;
    public int currentGun=1;
    public int currentBullets;
    

    // Update is called once per frame
    void Update()
    {
        MyInput();
        SwitchGun();
    }
    void MyInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentGun = 1;
            
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentGun = 2;
            
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentGun = 3;
            
        }
    }

    void SwitchGun()
    {
        switch (currentGun)
        {
            case 1:
                pistola.SetActive(true);
                rifle.SetActive(false);
                escopeta.SetActive(false);
                currentBullets = pistola.GetComponent<GunController>().bulletsLeft;
                return;
            case 2:
                pistola.SetActive(false);
                rifle.SetActive(true);
                escopeta.SetActive(false);
                currentBullets = rifle.GetComponent<GunController>().bulletsLeft;
                return;
            case 3:
                pistola.SetActive(false);
                rifle.SetActive(false);
                escopeta.SetActive(true);
                currentBullets = escopeta.GetComponent<GunController>().bulletsLeft/escopeta.GetComponent<GunController>().bulletsPerTap;
                return;
        }
    }
}
