﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombaTactica : MonoBehaviour
{
    public bool isActive;
    public float damage=50f,range=15;
    public BasicEnemy enemy;

    
    public SphereCollider explosionBlast;
    public bool goingToExplode;
    public float explosionTimer,timeToExplode;

    public GameObject explosionEffect;
    
    void Start()
    {
        enemy = GetComponent<BasicEnemy>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        Arm();
        Explode();
        enemy.Die();
    }

    void Arm()
    {
        if (enemy.distanceToPlayer <= range)
        {
            enemy.Chase();
            enemy.agent.speed = 9f;
            enemy.agent.isStopped = false;
            
        }
        else if(enemy.distanceToPlayer>=range&&!goingToExplode)
        {
            enemy.agent.isStopped = true;
            
        }
    }

    void Explode()
    {
        if (enemy.distanceToPlayer <= 5f)
        {
            goingToExplode = true;
        }
        else
            goingToExplode = false;
        if (goingToExplode)
        {
            explosionTimer += Time.deltaTime;
            if (explosionTimer >= timeToExplode)
            {

                GameObject obj = Instantiate(explosionEffect);
                obj.transform.position = transform.position + Vector3.up * 2;
                Destroy(gameObject);
            }
            if (explosionTimer >= timeToExplode - 0.2f)
            {
                explosionBlast.enabled = true;
            }
        }
        else
            explosionTimer = 0;
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().health -= damage;
            other.GetComponentInParent<PlayerController>().damageTaken += damage;
        }
    }
}
