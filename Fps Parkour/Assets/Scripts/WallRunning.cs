﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRunning : MonoBehaviour
{
    public LayerMask whatIsWall;
    public LayerMask whatIsGround;
    public float wallRunForce, maxWallRunTime, maxWallSpeed;
    public bool wallRunRight, wallRunLeft, isWallRunning;

    public float maxCameraTilt, cameraTilt;

    public Camera mainCamera;
    public Transform orientation;
    private float basicFieldOfView;
    private float distanceToGround;
    private Vector3 hitGoundImpactPoint;
    public RaycastHit hitToGround;
    public Rigidbody rb;

    public PlayerController pc;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        basicFieldOfView = mainCamera.fieldOfView;
    }
   

    private void MyInput()
    {
        if (Input.GetKey(KeyCode.W) && wallRunRight && distanceToGround > 3f)
        {
            StartWallRun();
        }else if (!Input.GetKey(KeyCode.W))
        {
            isWallRunning = false;
            StopWallRun();
        }
        
        if (Input.GetKey(KeyCode.W) && wallRunLeft && distanceToGround > 3f)
        {
            StartWallRun();
        }
        else if (!Input.GetKey(KeyCode.W))
        {
            isWallRunning = false;
            StopWallRun();
        }

    }
    
    public void StartWallRun()
    {
        
        rb.useGravity = false;
        isWallRunning = true;
        if (mainCamera.fieldOfView < basicFieldOfView + 20&&!pc.isSliding)
        {
            mainCamera.fieldOfView += 100 * Time.deltaTime;
        }
        
        if (rb.velocity.magnitude <= maxWallSpeed)
        {
            rb.AddForce(orientation.forward * wallRunForce * Time.deltaTime);

            if (wallRunLeft)
                rb.AddForce(-orientation.right * wallRunForce / 5 * Time.deltaTime);
            else if(wallRunRight)
                rb.AddForce(orientation.right * wallRunForce / 5 * Time.deltaTime);
        }

        if (!Input.GetKey(KeyCode.W))
        {
            StopWallRun();
            isWallRunning = false;
        }

       
    }
    public void StopWallRun()
    {
        if (mainCamera.fieldOfView > basicFieldOfView&&!pc.isSliding)
        {
            mainCamera.fieldOfView -= 100 * Time.deltaTime;
        }
        
        rb.useGravity = true;
        isWallRunning = false;
    }

    private void WallCheck()
    {
        wallRunRight = Physics.Raycast(transform.position, orientation.right, 1.5f, whatIsWall);
        wallRunLeft = Physics.Raycast(transform.position, -orientation.right, 1.5f, whatIsWall);

        if (!wallRunLeft && !wallRunRight)
        {
            StopWallRun();
        }
    }

    private void DistanceToGround()
    {
        Physics.Raycast(transform.position, -orientation.up, out hitToGround, 999f, whatIsGround);
        
        hitGoundImpactPoint = hitToGround.transform.position;

        distanceToGround = Mathf.Abs(hitGoundImpactPoint.y - orientation.position.y);

        
    }

    public void FixedUpdate()
    {
        MyInput();
        WallCheck();
        PerformCameraTilt();
        DistanceToGround();
    }

    public void PerformCameraTilt()
    {
        if (Mathf.Abs(cameraTilt) < maxCameraTilt && isWallRunning && wallRunRight)
            cameraTilt += Time.deltaTime * maxCameraTilt * 2;
        if (Mathf.Abs(cameraTilt) < maxCameraTilt && isWallRunning && wallRunLeft)
            cameraTilt -= Time.deltaTime * maxCameraTilt * 2;

        if (cameraTilt > 0 && !wallRunRight && !wallRunLeft)
            cameraTilt -= Time.deltaTime * maxCameraTilt * 2;
        if (cameraTilt < 0 && !wallRunRight && !wallRunLeft)
            cameraTilt += Time.deltaTime * maxCameraTilt * 2;
    }

}
