﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatTacker : MonoBehaviour
{
    public BasicEnemy enemy;
    public float embestirCooldown, embestidaDuration,embestirTimer;
    public bool embistiendo, closeToPlayer;
    public float dañoEmbestida;

    public bool isRecovering;
    public float recoveryTimer;

    public LineRenderer laser;
    public Transform laserPoint;
    public RaycastHit laserHit;
    public float laserResetTimer;
    public float timeToStopLaser;
    public bool laserActive=false;

    public Animator animator;
    public bool isMoving;

    public bool playerSlowed;
    public float slowTimer;

  
    
    void Start()
    {
        
        enemy.agent.speed = 5f;
        animator = GetComponent<Animator>();
        laserActive = false;
        laserResetTimer=0;
        
    }
    private void Awake()
    {
        laser = GetComponent<LineRenderer>();
        laser.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isRecovering)
        {
            enemy.agent.isStopped = false;
            isMoving = true;
            enemy.Chase();
        }
        if (isRecovering)
        {
            isMoving = false;
            enemy.agent.isStopped = true;
        }
       
        
        Embestir();
        Recovey();
        SetAnimation();
        ResetPlayerSpeed();
        ActivateLaserAttack();
    }

    void SetAnimation()
    {
        animator.SetBool("isMoving", isMoving);
        animator.SetBool("Embistiendo", embistiendo);
        animator.SetBool("Recovering", isRecovering);
    }
    void Embestir()
    {
        if(!isRecovering&&!closeToPlayer)
            embestirTimer += Time.deltaTime;

        if (embestirTimer >= embestirCooldown)
        {
            
            enemy.agent.speed = enemy.normalSpeed * 2.5f;
            embistiendo = true;
            Invoke("StopEmbestida", embestidaDuration);
        }
    }
    void StopEmbestida()
    {
        
        embestirTimer = 0;
        enemy.agent.speed = enemy.normalSpeed;
        embistiendo = false;
    }

    void Recovey()
    {
        if (enemy.health <= 0)
        {
            isRecovering = true;
            laserActive = false;
            laser.enabled = false;
            enemy.health = enemy.maxHealth;
            Invoke("ResetRecovery", recoveryTimer);
        }
    }
    void ResetRecovery()
    {
        enemy.health = enemy.maxHealth;
        isRecovering = false;
    }
    void ActivateLaserAttack()
    {
        if (laserActive&&!isRecovering)
        {
            laser.enabled = true;
            Laser();
            timeToStopLaser += Time.deltaTime;
            if (timeToStopLaser >= 3 && !isRecovering)
            {
                timeToStopLaser = 0;
                laserActive = false;
            }
        }else if (!laserActive)
        {
            laser.enabled = false;
            laserResetTimer += Time.deltaTime;         
            if (laserResetTimer >= 10)
            {
                laserResetTimer = 0;
                laserActive = true;
            }
        }
    }
    void Laser()
    {
        laser.SetPosition(0, laserPoint.position);
        
        laser.SetPosition(1, laser.GetPosition(0)+laserPoint.transform.forward);
        Quaternion rotTarget = Quaternion.LookRotation(enemy.player.transform.position - laserPoint.position);
        laserPoint.rotation = Quaternion.RotateTowards(laserPoint.rotation, rotTarget, 3 * Time.deltaTime);

        if (Physics.Raycast(laserPoint.position, laserPoint.transform.forward, out laserHit))
        {
            if (laserHit.collider)
            {
                laser.SetPosition(1, laserHit.point);
            }
            if (laserHit.collider.CompareTag("Player"))
            {
                laserHit.collider.gameObject.GetComponent<PlayerController>().health -= 20*Time.deltaTime;
                laserHit.collider.gameObject.GetComponent<PlayerController>().damageTaken += 20 * Time.deltaTime;
            }

        }
        else
        {
            laser.SetPosition(1, transform.forward * 5000);
        }

        
    }
 
    void SlowPlayer()
    {
        enemy.player.GetComponent<PlayerController>().speed = enemy.player.GetComponent<PlayerController>().basicSpeed *0.5f;
        playerSlowed = true;
    }
    void ResetPlayerSpeed()
    {
        if (playerSlowed)
        {
            slowTimer += Time.deltaTime;
            if (slowTimer >= 4)
            {
                slowTimer = 0;
                enemy.player.GetComponent<PlayerController>().speed = enemy.player.GetComponent<PlayerController>().basicSpeed;
                playerSlowed = false;

            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            if (embistiendo)
            {
                collision.collider.GetComponent<PlayerController>().health -= dañoEmbestida;
                collision.collider.GetComponentInParent<PlayerController>().damageTaken += dañoEmbestida;
                StopEmbestida();
                SlowPlayer();
            }
            else
            {
                closeToPlayer = true;
            }
            
        }
    }
   

}
